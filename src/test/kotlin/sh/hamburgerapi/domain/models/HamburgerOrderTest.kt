package sh.hamburgerapi.domain.models

import com.appmattus.kotlinfixture.kotlinFixture
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import java.math.BigDecimal

class HamburgerOrderTest {

    val fixture = kotlinFixture()

    @Test
    fun `should calculate the correct price for an order`() {
        val firstTopping = fixture<Topping>().copy(price = BigDecimal("0.50"))
        val secondTopping = fixture<Topping>().copy(price = BigDecimal("0.25"))

        val patty = fixture<Patty>().copy(price = BigDecimal("1.00"))
        val bun = fixture<Bun>().copy(price = BigDecimal("2.10"))
        val menuOption = fixture<HamburgerMenuOption>().copy(
            basePrice = BigDecimal(4.00),
            allowedPatty = listOf(patty),
            maxNumberOfToppings = 2,
        )

        val order = HamburgerOrder.createOrder(
            menuOption,
            bun,
            patty,
            toppings = listOf(firstTopping, secondTopping),
        )

        order.totalPrice() shouldBe BigDecimal("7.85")
    }
}

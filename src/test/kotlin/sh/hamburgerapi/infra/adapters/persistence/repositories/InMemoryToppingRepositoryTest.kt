package sh.hamburgerapi.infra.adapters.persistence.repositories

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.collections.shouldHaveSize
import org.junit.jupiter.api.Test
import sh.hamburgerapi.domain.errors.HamburgerApiDomainException

class InMemoryToppingRepositoryTest {

    val sut: InMemoryToppingRepository = InMemoryToppingRepository()

    @Test
    fun `should throw an exception when at least one topping cannot be found`() {
        shouldThrow<HamburgerApiDomainException> { sut.getToppingsByIds(listOf("cheese", "notExistingTopping")) }
    }

    @Test
    fun `should return expected list of Toppings`() {
        sut.getToppingsByIds(listOf("cheese", "lettuce", "bacon")) shouldHaveSize 3
    }
}

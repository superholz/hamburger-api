package sh.hamburgerapi.application

import com.appmattus.kotlinfixture.kotlinFixture
import io.kotest.assertions.throwables.shouldThrow
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import sh.hamburgerapi.domain.models.Bun
import sh.hamburgerapi.domain.models.HamburgerMenuOption
import sh.hamburgerapi.domain.models.Patty
import sh.hamburgerapi.domain.models.Topping
import sh.hamburgerapi.domain.ports.BunRepository
import sh.hamburgerapi.domain.ports.HamburgerMenuOptionsRepository
import sh.hamburgerapi.domain.ports.PattyRepository
import sh.hamburgerapi.domain.ports.ToppingRepository

@ExtendWith(MockKExtension::class)
class OrderOneHamburgerTest(
    @MockK private val menuRepository: HamburgerMenuOptionsRepository,
    @MockK private val bunRepository: BunRepository,
    @MockK private val pattyRepository: PattyRepository,
    @MockK private val toppingRepository: ToppingRepository,
) {

    @InjectMockKs
    lateinit var sut: OrderOneHamburger

    val fixture = kotlinFixture()

    @BeforeEach
    fun setup() {
        every { menuRepository.getOneById(any()) } returns fixture<HamburgerMenuOption>()
        every { bunRepository.getOneById(any()) } returns fixture<Bun>()
        every { pattyRepository.getOneById(any()) } returns fixture<Patty>()
        every { toppingRepository.getToppingsByIds(any()) } returns fixture<List<Topping>>() { repeatCount { 2 } }
    }

    @Test
    fun `should throw InvalidPattyException when not allowed patty is ordered`() {
        val input = fixture<OrderOneHamburger.Input>()
            .copy(pattyId = "not_allowed_patty_id")

        shouldThrow<HamburgerMenuOption.InvalidPattyException> { sut.order(input) }
    }

    @Test
    fun `should throw TooManyToppingsException when too many toppings are requested`() {
        val allowedPatty = fixture<Patty>()
        every { toppingRepository.getToppingsByIds(any()) } returns fixture<List<Topping>>() { repeatCount { 4 } }
        every { menuRepository.getOneById(any()) } returns fixture<HamburgerMenuOption>()
            .copy(
                maxNumberOfToppings = 3,
                allowedPatty = listOf(allowedPatty),
            )
        every { pattyRepository.getOneById(allowedPatty.id) } returns allowedPatty

        val input = fixture<OrderOneHamburger.Input>().copy(pattyId = allowedPatty.id)

        shouldThrow<HamburgerMenuOption.TooManyToppingsException> { sut.order(input) }
    }
}

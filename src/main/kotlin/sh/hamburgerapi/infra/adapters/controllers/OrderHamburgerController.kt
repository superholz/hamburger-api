package sh.hamburgerapi.infra.adapters.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import sh.hamburgerapi.application.OrderOneHamburger
import sh.hamburgerapi.domain.models.HamburgerOrder
import java.math.BigDecimal

@Controller
class OrderHamburgerController(
    private val service: OrderOneHamburger,
) {
    @PostMapping("/orders")
    fun orderOneHamburger(
        @RequestBody body: OrderOneHamburgerRequest,
    ): ResponseEntity<OrderOneHamburgerResponse> {
        val order = service.order(body.toInput())
        val output = order.toOutput()

        return ResponseEntity(
            output,
            HttpStatus.CREATED,
        )
    }

    fun OrderOneHamburgerRequest.toInput() =
        OrderOneHamburger.Input(
            hamburgerOptionId = hamburgerMenuOptionId,
            bunId = bunId,
            pattyId = pattyId,
            toppingIds = toppingIds,
        )

    fun HamburgerOrder.toOutput(): OrderOneHamburgerResponse {
        val outPutToppings =
            toppings.map { OrderOneHamburgerResponse.Topping(name = it.name, price = it.price) }

        val drink = hamburgerMenuOption.combinationDishes?.drink
        val sideDish = hamburgerMenuOption.combinationDishes?.sideDish

        return OrderOneHamburgerResponse(
            menuOption = OrderOneHamburgerResponse.MenuOption(
                name = hamburgerMenuOption.name,
                basePrice = hamburgerMenuOption.basePrice,
            ),
            bun = OrderOneHamburgerResponse.Bun(name = bun.name, price = bun.price),
            patty = OrderOneHamburgerResponse.Patty(name = patty.name, price = patty.price),
            toppings = OrderOneHamburgerResponse.Toppings(toppings = outPutToppings),
            totalPrice = totalPrice(),
            drink = if (drink != null) {
                OrderOneHamburgerResponse.Drink(drink.name)
            } else {
                null
            },
            sideDish = if (sideDish != null) {
                OrderOneHamburgerResponse.SideDish(sideDish.name)
            } else {
                null
            },
        )
    }
}

data class OrderOneHamburgerRequest(
    val hamburgerMenuOptionId: String,
    val bunId: String,
    val pattyId: String,
    val toppingIds: List<String>,
)

data class OrderOneHamburgerResponse(
    val menuOption: MenuOption,
    val bun: Bun,
    val patty: Patty,
    val toppings: Toppings,
    val drink: Drink? = null,
    val sideDish: SideDish? = null,
    val totalPrice: BigDecimal,
) {

    data class MenuOption(
        val name: String,
        val basePrice: BigDecimal,
    )

    data class Bun(
        val name: String,
        val price: BigDecimal,
    )

    data class Patty(
        val name: String,
        val price: BigDecimal,
    )

    data class Toppings(
        val toppings: List<Topping>,
    )

    data class Topping(
        val name: String,
        val price: BigDecimal,
    )

    data class Drink(
        val name: String,
    )

    data class SideDish(
        val name: String,
    )
}

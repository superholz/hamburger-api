package sh.hamburgerapi.infra.adapters.controllers

import org.springframework.http.HttpStatus
import org.springframework.http.ProblemDetail
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import sh.hamburgerapi.domain.errors.HamburgerApiDomainException

@RestControllerAdvice(basePackages = ["sh.hamburgerapi.infra.adapters.controllers"])
class ControllerAdvise {

    @ExceptionHandler(value = [HamburgerApiDomainException::class])
    fun handleDomainException(exception: HamburgerApiDomainException) =
        ProblemDetail.forStatus(HttpStatus.BAD_REQUEST)
}

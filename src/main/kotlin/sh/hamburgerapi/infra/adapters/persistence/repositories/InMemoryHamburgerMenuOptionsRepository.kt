package sh.hamburgerapi.infra.adapters.persistence.repositories

import org.springframework.stereotype.Repository
import sh.hamburgerapi.domain.errors.HamburgerApiDomainException
import sh.hamburgerapi.domain.models.Drink
import sh.hamburgerapi.domain.models.HamburgerMenuOption
import sh.hamburgerapi.domain.models.Patty
import sh.hamburgerapi.domain.models.SideDish
import sh.hamburgerapi.domain.ports.HamburgerMenuOptionsRepository
import java.math.BigDecimal

@Repository
class InMemoryHamburgerMenuOptionsRepository : HamburgerMenuOptionsRepository {
    override fun getOneById(id: String): HamburgerMenuOption =
        menuOptions.firstOrNull { it.id == id } ?: throw HamburgerApiDomainException(
            "Menu option with id $id could not be found.",
        ).also { println(it) }

    private val menuOptions = listOf(
        HamburgerMenuOption(
            id = "basic",
            name = "Basic",
            maxNumberOfToppings = 4,
            allowedPatty = allPatties,
            basePrice = BigDecimal("5.00"),
        ),
        HamburgerMenuOption(
            id = "healthy",
            name = "Healthy",
            maxNumberOfToppings = 3,
            allowedPatty = listOf(
                Patty("vegan", "Vegan Patty", BigDecimal("0.00")),
                Patty("fish", "Fish Patty", BigDecimal("0.00")),
            ),
            basePrice = BigDecimal("4.00"),
        ),
        HamburgerMenuOption(
            id = "combo",
            name = "Combo",
            maxNumberOfToppings = 4,
            allowedPatty = allPatties,
            basePrice = BigDecimal("5.00"),
            combinationDishes = HamburgerMenuOption.CombinationDishes(Drink(), SideDish()),
        ),
    )

    companion object {
        private val allPatties = listOf(
            Patty("angus", "Black angus patty", BigDecimal("2.00")),
            Patty("kobe", "Kobe beef patty", BigDecimal("2.00")),
            Patty("vegan", "Vegan Patty", BigDecimal("0.00")),
            Patty("fish", "Fish Patty", BigDecimal("0.00")),
        )
    }
}

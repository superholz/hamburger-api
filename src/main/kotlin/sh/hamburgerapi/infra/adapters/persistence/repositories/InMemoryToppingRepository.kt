package sh.hamburgerapi.infra.adapters.persistence.repositories

import org.springframework.stereotype.Repository
import sh.hamburgerapi.domain.errors.HamburgerApiDomainException
import sh.hamburgerapi.domain.models.Topping
import sh.hamburgerapi.domain.ports.ToppingRepository
import java.math.BigDecimal

@Repository
class InMemoryToppingRepository : ToppingRepository {
    override fun getToppingsByIds(ids: List<String>): List<Topping> {
        ids.validateAllToppingsExist()
        return toppings.filter { it.id in ids }
    }

    private fun List<String>.validateAllToppingsExist() {
        this.forEach {
            if (it !in toppings.map { topping -> topping.id }) {
                throw HamburgerApiDomainException("Topping with id $it could not be found.").also { exception ->
                    println(exception)
                }
            }
        }
    }

    private val toppings = listOf(
        Topping("cheese", "Cheese", BigDecimal("0.10")),
        Topping("onion", "Onion", BigDecimal("0.05")),
        Topping("tomato", "Tomato", BigDecimal("0.05")),
        Topping("ketchup", "Ketchup", BigDecimal("0.20")),
        Topping("mayo", "Mayonnaise", BigDecimal("0.15")),
        Topping("cheddar", "Cheddar cheese", BigDecimal("0.50")),
        Topping("lettuce", "Lettuce", BigDecimal("0.20")),
        Topping("bacon", "Bacon", BigDecimal("0.50")),
    )
}

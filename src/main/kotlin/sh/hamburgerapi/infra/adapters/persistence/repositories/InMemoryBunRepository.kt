package sh.hamburgerapi.infra.adapters.persistence.repositories

import org.springframework.stereotype.Repository
import sh.hamburgerapi.domain.errors.HamburgerApiDomainException
import sh.hamburgerapi.domain.models.Bun
import sh.hamburgerapi.domain.ports.BunRepository
import java.math.BigDecimal

@Repository
class InMemoryBunRepository : BunRepository {
    override fun getOneById(id: String) = buns.firstOrNull { it.id == id }
        ?: throw HamburgerApiDomainException("Bun with id '$id' could not be found").also { println(it) }

    private val buns = listOf(
        Bun("wholeGrain", "Whole Grain Bun", BigDecimal("0.00")),
        Bun("white", "White Bun", BigDecimal("0.00")),
        Bun("brioche", "Brioche", BigDecimal("1.00")),
        Bun("ciabatta", "Ciabatta", BigDecimal("1.50")),

    )
}

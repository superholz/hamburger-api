package sh.hamburgerapi.infra.adapters.persistence.repositories

import org.springframework.stereotype.Repository
import sh.hamburgerapi.domain.errors.HamburgerApiDomainException
import sh.hamburgerapi.domain.models.Patty
import sh.hamburgerapi.domain.ports.PattyRepository
import java.math.BigDecimal

@Repository
class InMemoryPattyRepository : PattyRepository {
    override fun getOneById(id: String) = patties.firstOrNull { it.id == id } ?: throw HamburgerApiDomainException("Patty with id '$id' could not be found").also { println(it.message) }

    val patties = listOf(
        Patty("angus", "Black angus patty", BigDecimal("2.00")),
        Patty("kobe", "Kobe beef patty", BigDecimal("2.00")),
        Patty("vegan", "Vegan Patty", BigDecimal("0.00")),
        Patty("fish", "Fish Patty", BigDecimal("0.00")),

    )
}

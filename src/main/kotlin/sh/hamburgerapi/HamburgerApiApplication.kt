package sh.hamburgerapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HamburgerApiApplication

fun main(args: Array<String>) {
    runApplication<HamburgerApiApplication>(*args)
}

package sh.hamburgerapi.application

import org.springframework.stereotype.Service
import sh.hamburgerapi.domain.models.HamburgerOrder
import sh.hamburgerapi.domain.ports.BunRepository
import sh.hamburgerapi.domain.ports.HamburgerMenuOptionsRepository
import sh.hamburgerapi.domain.ports.PattyRepository
import sh.hamburgerapi.domain.ports.ToppingRepository

@Service
class OrderOneHamburger(
    private val menuOptionsRepository: HamburgerMenuOptionsRepository,
    private val bunRepository: BunRepository,
    private val pattyRepository: PattyRepository,
    private val toppingRepository: ToppingRepository,
) {

    data class Input(
        val hamburgerOptionId: String,
        val bunId: String,
        val pattyId: String,
        val toppingIds: List<String> = emptyList(),
    )

    fun order(input: Input): HamburgerOrder {
        val chosenMenuItem = menuOptionsRepository.getOneById(input.hamburgerOptionId)

        val bun = bunRepository.getOneById(input.bunId)
        val patty = pattyRepository.getOneById(input.pattyId)
        val toppings = toppingRepository.getToppingsByIds(input.toppingIds)

        return HamburgerOrder.createOrder(
            hamburgerMenuOption = chosenMenuItem,
            bun = bun,
            patty = patty,
            toppings = toppings,
        )
    }
}

package sh.hamburgerapi.domain.errors


open class HamburgerApiDomainException(
    override val message: String?,
) : RuntimeException(message)

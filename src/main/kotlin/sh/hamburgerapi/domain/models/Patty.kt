package sh.hamburgerapi.domain.models

import java.math.BigDecimal

data class Patty(
    val id: String,
    val name: String,
    val price: BigDecimal,
)

package sh.hamburgerapi.domain.models

import sh.hamburgerapi.domain.errors.HamburgerApiDomainException
import java.math.BigDecimal

data class HamburgerMenuOption(
    val id: String,
    val name: String,
    val maxNumberOfToppings: Int,
    val allowedPatty: List<Patty>,
    val basePrice: BigDecimal,
    val combinationDishes: CombinationDishes? = null,
) {

    data class CombinationDishes(
        val drink: Drink,
        val sideDish: SideDish,
    )

    class InvalidPattyException(id: String) : HamburgerApiDomainException("Patty with id '$id' is not allowed for this Hamburger")
    class TooManyToppingsException(allowedNumberOfToppings: Int, reqestedNumberOfToppings: Int) : HamburgerApiDomainException(
        "Only $allowedNumberOfToppings number of toppings are allowed but $reqestedNumberOfToppings where requested.",
    )
}

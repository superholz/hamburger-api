package sh.hamburgerapi.domain.models

import java.math.BigDecimal

data class Bun(
    val id: String,
    val name: String,
    val price: BigDecimal,
)

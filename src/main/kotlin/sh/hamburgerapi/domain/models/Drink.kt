package sh.hamburgerapi.domain.models

data class Drink(
    val id: String = "comboDrink",
    val name: String = "Drink of choice",
)

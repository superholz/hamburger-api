package sh.hamburgerapi.domain.models

data class HamburgerOrder private constructor(
    val hamburgerMenuOption: HamburgerMenuOption,
    val bun: Bun,
    val patty: Patty,
    val toppings: List<Topping> = emptyList(),
) {
    fun totalPrice() = hamburgerMenuOption.basePrice + bun.price + patty.price + toppings.sumOf { it.price }

    companion object {
        fun createOrder(
            hamburgerMenuOption: HamburgerMenuOption,
            bun: Bun,
            patty: Patty,
            toppings: List<Topping>,
        ): HamburgerOrder {
            if (patty !in hamburgerMenuOption.allowedPatty) {
                throw HamburgerMenuOption.InvalidPattyException(patty.id)
                    .also { println(it) }
            }
            if (hamburgerMenuOption.maxNumberOfToppings < toppings.size) {
                throw HamburgerMenuOption.TooManyToppingsException(
                    allowedNumberOfToppings = hamburgerMenuOption.maxNumberOfToppings,
                    reqestedNumberOfToppings = toppings.size,
                ).also { println(it) }
            }

            return HamburgerOrder(
                hamburgerMenuOption = hamburgerMenuOption,
                bun = bun,
                patty = patty,
                toppings = toppings,
            )
        }
    }
}

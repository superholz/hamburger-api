package sh.hamburgerapi.domain.models

data class SideDish(
    val id: String = "comboFries",
    val name: String = "Fries",
)

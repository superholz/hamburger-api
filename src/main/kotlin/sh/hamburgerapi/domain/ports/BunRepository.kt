package sh.hamburgerapi.domain.ports

import sh.hamburgerapi.domain.models.Bun

interface BunRepository {
    fun getOneById(id: String): Bun
}

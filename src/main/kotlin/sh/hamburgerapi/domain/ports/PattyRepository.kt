package sh.hamburgerapi.domain.ports

import sh.hamburgerapi.domain.models.Patty

interface PattyRepository {
    fun getOneById(id: String): Patty
}

package sh.hamburgerapi.domain.ports

import sh.hamburgerapi.domain.models.HamburgerMenuOption

interface HamburgerMenuOptionsRepository {
    fun getOneById(id: String): HamburgerMenuOption
}

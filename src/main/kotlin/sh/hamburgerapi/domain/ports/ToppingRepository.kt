package sh.hamburgerapi.domain.ports

import sh.hamburgerapi.domain.models.Topping

interface ToppingRepository {
    fun getToppingsByIds(ids: List<String>): List<Topping>
}

# Hamburger Api #

This project is a small example how to implement a REST Api with spring boot.
With this api you can order a hamburger by an http request and receive the items ordered and the total price in response.

After starting the application try e.g.

POST http://localhost:8080/orders

Content-Type: application/json

with json body...

- To receive proper response for a combo burger with having fries and a drink added:

```json
{
   "hamburger_menu_option_id": "combo",
   "bun_id": "white",
   "patty_id": "angus",
   "topping_ids": [
      "lettuce", "cheese", "onion"
  ]
}
```

- To receive proper response basic burger:

```json
{
   "hamburger_menu_option_id": "basic",
   "bun_id": "ciabatta",
   "patty_id": "angus",
   "topping_ids": [
      "lettuce", "cheese", "onion"
  ]
}
```

- To see an response of a domain exception occuring that is printed and mapped to a bad request response because of too many toppings:

```json
{
   "hamburger_menu_option_id": "healthy",
   "bun_id": "ciabatta",
   "patty_id": "fish",
   "topping_ids": [
      "lettuce", "cheese", "onion", "tomato"
  ]
}
```

## Remarks on implementation ##

*In this example I wanted to show how hexagonal design can be used to separate different concerns of the application especially separating the Domain logic from the other parts of the application.
Therefore the application is divide into packages and different models are used for domain layer and e.g. controllers. For this size of the application this makes it complexer than needed; a much simpler structure would have been enough to cover the requirenment. So this is more to show the principles than to match exactly tooling to the task.*

*Further repositories are implemented in-memory. No database involved. The hexagonal design makes it easy to re-implement the repository ports in adapters that would fetch data from a database e.g..*

**Next steps if this was a real project**
- try this prototype with stakeholders to check if requirements are met so far
- add api authorisation e.g. by utilising spring security
- cover more of the functionalities by tests (e.g. add slice test to check for correct mapping in controller and add integration test to test correct wiering of the application)
- refactor and replace ids by UUID, using strings makes it just a bit more convenient to have proper test data


## Requirements ##

Create three different types of hamburgers

- Basic (€5.00)
- Healthy (€4.00)
- Combo (€5.00)

All burgers should have one bread type and one type of patty.

The basic hamburger should have the following items:

- Base basic burger price
- Up to 4 toppings (e.g. cheese, sauces, lettuce, tomato, onion, etc) each with their own individual prices

Potential types of buns with their own individual prices:

- Wholegrain (+€0)
- White (+€0)
- Brioche (+€1.00)
- Ciabatta (+€1.50)

Potential patty choices with their own individual prices:

- Black angus patty (+€2.00)
- Kobe beef patty (+€2.00)
- Vegan patty (+€0)
- Fish patty (+€0)

**The healthy burger**

- Base healthy burger price
- Can have up to 3 toppings
- Can only have the vegan or fish patty
- Everything else that’s allowed for the basic hamburger

**The combo burger**

- Base combo burger price
- Comes with chips and drinks
- Everything else that’s allowed for the basic hamburger

**It should be possible to order a burger via API (e.g. REST)**
**Returns the price for the burger, and the price of each individual component of the burger.**


